'use strict'

const fp = require('fastify-plugin')
const ProjectService = require("../api/projects/service");
const UserService = require('../api/users/service')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async function (fastify, opts) {
    const projectService = new ProjectService(fastify.knex, 'projects')
    const userService = new UserService(fastify.knex, 'users')

    fastify.decorate('projectService', projectService)
    fastify.decorate('userService', userService)
})


