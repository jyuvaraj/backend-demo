'use strict'

const fp = require('fastify-plugin')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async function (fastify) {
    fastify
        .register(require('fastify-knexjs'), {
            client: 'pg',
            connection: 'postgres://yuv:dummytrain@localhost:5432/timesheet',
            pool: {
                min: 0,
                max: 10,
                idleTimeoutMillis: 5000,
            },
        }, err => console.log(err))
})

