'use strict'

const fp = require('fastify-plugin')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async function (fastify, opts) {
    fastify.decorate('adminPreHandler', (request, reply, done) => {
        if (!request.user.isAdmin) {
            reply.send({
                'error': 'Unauthorized',
                'message': 'You are unauthorized to access this route',
                'statusCode': 403
            })
        }
        done()
    })
})

