'use strict'

const fp = require('fastify-plugin')

// the use of fastify-plugin is required to be able
// to export the decorators to the outer scope

module.exports = fp(async function (fastify, opts) {
    fastify.register(require('fastify-jwt'), {
            secret: fastify.config.JWT_SECRET,
            algorithms: ['RS256']
        })

    fastify.decorate('authPreHandler', async function auth(request, reply) {
        try {
            await request.jwtVerify()
        } catch (e) {
            reply.send(e)
        }
    })
})

