'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')

const schema = {
    type: 'object',
    required: ['JWT_SECRET'],
    properties: {
        JWT_SECRET: { type: 'string' }
    },
    additionalProperties: false
};
module.exports = async function (fastify, opts) {
    fastify.register(require('fastify-env'), { schema, dotenv: true })

    // This loads all plugins defined in plugins
    // those should be support plugins that are reused
    // through your application
    fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
    })

    fastify
        // APIs modules
        .register(require('./api/projects'), { prefix: '/api/projects' })
        .register(require('./api/users'), { prefix: '/api/users' })
}
