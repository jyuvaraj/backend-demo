'use strict'

const {
    create: projectCreateSchema,
    list: projectListSchema,
} = require('./schemas')

module.exports = async function(fastify, opts) {
    // Authenticated only
    fastify.register(async function (fastify) {
        fastify.addHook('preHandler', fastify.authPreHandler)

        // Admin only
        fastify.register(async function(fastify) {
            fastify.addHook('preHandler', fastify.adminPreHandler)
            fastify.post('/', { schema: projectCreateSchema }, createHandler)
        })

        fastify.get('/', { schema: projectListSchema }, listHandler)
    })
}

module.exports[Symbol.for('plugin-meta')] = {
    decorators: {
        fastify: [
            'knex',
            'authPreHandler',
            'projectService'
        ]
    }
}

async function createHandler(request, reply) {
    const { name } = request.body
    const projectId = await this.projectService.create(name)
    return { projectId }
}

async function listHandler(request, reply) {
    const projects = await this.projectService.list()
    return { projects }
}