'use strict'

const projectObject = {
    type: 'object',
    required: ['_id', 'name'],
    properties: {
        _id: { type: 'string' },
        name: { type: 'string' }
    }
}

const projectCreateSchema = {
    body: {
        type: 'object',
        required: ['name'],
        properties: {
            name: {type: 'string'},
        },
        additionalProperties: false
    },
    response: {
        200: {
            type: 'object',
            required: [ 'projectId' ],
            properties: {
                projectId: { type: 'string' }
            },
            additionalProperties: false
        }
    }
}

const projectListSchema = {
    response: {
        200: {
            type: 'array',
            items: projectObject
        }
    }
}

module.exports = {
    projectCreateSchema,
    projectListSchema,
}