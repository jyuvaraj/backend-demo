'use strict'

class ProjectService {
    constructor(knex, tableName) {
        this.knex = knex
        this.tableName = tableName
    }

    create = async (name) => {
        let writeResult
        writeResult = await this.knex(this.tableName)
            .returning('id')
            .insert({name: name})
            // .then((writtenId) => {
            //     writeResult = writtenId
            //     reply.send(201).send(writtenId)
            //     console.log(writeResult)
            // })
            // .catch((err) => { console.log(err); throw err })
        console.log(writeResult)
        return writeResult
    }

    list = async () => {
        let writeResult
        writeResult = await this.knex
            .select()
            .from(this.tableName)
        return writeResult
    }
}

module.exports = ProjectService