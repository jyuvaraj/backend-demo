'use strict'

const userObject = {
    type: 'object',
    required: ['username', 'password'],
    properties: {
        username: { type: 'string' },
        password: { type: 'string' }
    },
    additionalProperties: false
}

const userCreateSchema = {
    body: userObject,
    response: {
        200: {
            type: 'object',
            required: [ 'userId' ],
            properties: {
                userId: { type: 'string' }
            },
            additionalProperties: false
        }
    }
}

const userLoginSchema = {
    body: userObject,
    response: {
        200: {
            type: 'object',
            require: [ 'jwt' ],
            properties: {
                jwt: { type: 'string' }
            },
            additionalProperties: false
        }
    }
}

module.exports = {
    userCreateSchema,
    userLoginSchema
}