'use strict'

const {
    create: userCreateSchema,
    login: userLoginSchema
} = require('./schemas')

module.exports = async function(fastify, opts) {
    fastify.post('/', { schema: userCreateSchema }, createHandler)
    fastify.post('/login', { schema: userLoginSchema }, loginHandler)
}

module.exports[Symbol.for('plugin-meta')] = {
    decorators: {
        fastify: [
            'knex',
            'bcrypt',
            'userService'
        ]
    }
}

async function createHandler(request, reply) {
    const { username, password } = request.body
    try {
        const userId = await this.userService.create(username, password, this.bcrypt.hashSync)
        if (!userId) {
            throw 'Something went wrong'
        }
        return { userId }
    } catch (e) {
        console.error(e)
        if (e.code === '23505') {
            return {
                error: "Username Already Exists",
                message: "This username already exists",
                statusCode: 409
            }
        }
        return {
            statusCode: 500,
            message: "Something went wrong"
        }
    }
}

async function loginHandler(request, reply) {
    const { username, password } = request.body
    console.log(username, password)
    const user = await this.userService.login(username, password, this.bcrypt.compareSync)
    return { jwt: this.jwt.sign(user) }
}
