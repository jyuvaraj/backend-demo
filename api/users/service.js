'use strict'

class UserService {
    constructor(knex, tableName) {
        this.knex = knex
        this.tableName = tableName
    }

    create = async (username, password, hashFn) => {
        const passwordHash = await hashFn(password, 10)
        return this.knex(this.tableName)
            .returning('id')
            .insert({username: username, password: passwordHash});
    }


    login = async (username, password, hashCompareFn) => {
        let result = await this.knex
            .select('id', 'username', 'password', 'is_admin')
            .from(this.tableName)
            .where({ username: username })

        if (result.length <= 0) {
            throw 'Wrong credentials'
        }
        let user = result[0]
        if (!(hashCompareFn(password, result[0].password))) {
            throw 'Wrong credentials'
        }
        return {
            id: user.id,
            username: user.username,
            isAdmin: user.is_admin
        }
    }

    isAdmin = (user) => {
        return user.isAdmin
    }
}

module.exports = UserService